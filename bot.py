#!/usr/bin/env python3
import discord, os, asyncio, time, random, string, re, math, sys
from datetime import datetime
from dateutil import tz
import bordraw

fut = asyncio.ensure_future

file_path = os.path.abspath(__file__)
folder = os.path.dirname(file_path)
bordraw.folder = folder

TIME_FORMAT = "%F %T"


if not os.path.exists(folder + "/logs"):
	os.mkdir(folder + "/logs")

logfile = folder + "/logs/{}.log".format(time.strftime("%F"))
num = 1
while os.path.exists(logfile):
	logfile = folder + "/logs/{}-{}.log".format(time.strftime("%F"), num)
	num += 1

def log(msg, debug=False):
	if debug:
		if debugMode:
			fullMsg = "[{}] [DEBUG] {}".format(time.strftime(TIME_FORMAT), msg)
		else:
			return
	else:
		fullMsg = "[{}] {}".format(time.strftime(TIME_FORMAT), msg)
	print(fullMsg)
	with open(logfile, "a") as f:
		f.write(fullMsg + "\n")

async def send(channel, msg, files=None):
	if channel.type == discord.ChannelType.private:
		recipient = channel.recipient
		name = f"{recipient.name}#{recipient.discriminator}"
	else:
		name = channel.name
		
	if files is None or files == []:
		log("#{}: {}".format(name, msg))
	else:
		filenames = []
		for f in files:
			filenames.append(f.filename)
		log("#{}: {} [{}]".format(name, msg, ", ".join(filenames)))

	try:
		await channel.send(msg, files=files)
	except Exception as e:
		log("Message failed to send: " + e)

async def delete(msg, files=None):
	if msg.attachments is None or msg.attachments == []:
		log("Deleting from #{}: <{}#{}> {}".format(msg.channel.name, msg.author.name, msg.author.discriminator, msg.content))
	else:
		filenames = []
		for f in msg.attachments:
			filenames.append(f.filename)
		log("Deleting from #{}: <{}#{}> {} [{}]".format(msg.channel.name, msg.author.name, msg.author.discriminator, msg.content, ", ".join(filenames)))
	await msg.delete()

if len(sys.argv) > 1 and sys.argv[1] == "debug":
	debugMode = True
	log("Starting Maidbot in debug mode...")
else:
	debugMode = False
	log("Starting Maidbot...")

client = discord.Client()
bordraw.client = client

token = None
try:
	with open("{}/token".format(folder)) as token_file:
		token = token_file.read().strip("\n")
except:
	log("Couldn't get token")

if not os.path.isdir("{}/channels".format(folder)):
	os.makedirs("{}/channels".format(folder))

channels = {"bordraw": None, "moon": None, "earth": None, "box": None, "people": None, "peoplediscussion": None}
ids = {}
for channel in channels:
	try:
		with open("{}/channels/{}".format(folder, channel)) as f:
			ids[channel] = int(f.read())
	except:
		log("Couldn't get {} channel".format(channel))

# Message from uakci: this probably should've been made into a role.
# Hardcoding the administrators' IDs sounds like a terrible idea.
	   #Iykury			  Alexolas			   uakci
mods = [201874260787068928, 266358272150339584] #, 221638066740133890]

box_role = "📦"
box_futures = {}

peopleMsg = None

startup = True

@client.event
async def on_ready():
	global startup
	log("Logged in as")
	log(client.user.name)
	log(client.user.id)
	log("------")
	log(time.strftime(TIME_FORMAT), True)
	log("startup = " + str(startup), True)
	log("------", True)
	for key, channel in channels.items():
		if channel is None:
			try:
				channels[key] = client.get_channel(ids[key])
			except:
				log("Couldn't get {} channel".format(key))
	if bordraw.channel is None:
		bordraw.channel = channels["bordraw"]

	initialize_roles()
	
	if startup:
		asyncio.gather(
			earth_name(),
			moon_name(),
			moon_chat(),
			update_tz_roles())
		startup = False

@client.event
async def on_message(message):
	#Bot won't reply to itself
	if message.author == client.user:
		return

	#Anti-uagle-whitespace-spamming code
	whitespace = "\x09\x0a\x0b\x0c\x0d\x20\x85\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\
\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000\u180e\u200b\u200c\u200d\u2060\ufeff\uffa0\u2800"
	spaceCount = 0
	for char in message.content:
		if char in whitespace:
			spaceCount += 1
	if len(message.content) > 10 and spaceCount > len(message.content) * 0.75:
		await delete(message)
		if message.author in box_futures:
			box_futures[message.author].cancel
			box_futures.pop(message.author)
		box_futures[message.author] = fut(box(message.author))
		return
	
	parts = message.content.split(" ")
	command = None
	if parts[0].startswith("!"):
		command = parts[0][1:]
	
	if command == "channel":
		if not message.author.id in mods:
			await send(message.channel, "Only Iykury and Alexolas are allowed to change the channel I'm on")
			return
		if not parts[1] in channels:
			await send(message.channel, "{} isn't a valid channel".format(parts[1]))
			return
		last_channel = message.channel
		if not channels[parts[1]] is None:
			last_channel = channels[parts[1]]
		await send(last_channel, "{0} is now on <#{1.channel.id}>".format(parts[1].capitalize(), message))
		if parts[1] == "bordraw":
			bordraw.channel = message.channel
		channels[parts[1]] = message.channel
		ids[parts[1]] = message.channel.id
		with open("{}/channels/{}".format(folder, parts[1]), "w") as f:
			f.write(str(ids[parts[1]]))
	
	if command == "pack" or command == "box":
		if message.author.id not in mods:
			await send(message.channel, "Only Iykury and Alexolas are allowed to box people")
			return
		if message.mentions is None or len(message.mentions) == 0:
			await send(message.channel, "Specify one or more users")
		for member in message.mentions:
			if member in box_futures:
				box_futures[member].cancel
				box_futures.pop(member)
			box_futures[member] = fut(box(member))

	if command == "unpack" or command == "unbox":
		if message.author.id not in mods:
			await send(message.channel, "Only Iykury and Alexolas are allowed to unbox people")
			return
		if message.mentions is None or len(message.mentions) == 0:
			await send(message.channel, "Specify one or more users")
		for member in message.mentions:
			fut(unbox(member))
	
	if command == "xd":
		await send(message.channel, "<a:xd:696396430692057128>")
	
	randomNgeng = random.randrange(100) == 0 and command != "ngeng" #1% of messages will be ngenged automatically
	if command == "ngeng" or randomNgeng:
		if randomNgeng:
			text = message.content
		else:
			text = " ".join(parts[1:])
		upper = "NGE"
		lower = "nge"
		ngeng = ""

		if text == "" and not randomNgeng:
			isNext = False
			async for m in message.channel.history(limit=100):
				if isNext:
					text = m.content
					break
				if m.id == message.id:
					isNext = True

		charcount = 0
		for char in text:
			if not char in string.ascii_letters:
				charcount = -1
				ngeng += char
			elif char in string.ascii_uppercase:
				ngeng += upper[charcount % 3]
			elif char in string.ascii_lowercase:
				ngeng += lower[charcount % 3]

			charcount += 1

		if ngeng: #If it tries to randomly ngeng an empty message, this check will fail
			await send(message.channel, ngeng)
	
	if command == "roll":
		args = parts[1].split("d")

		try:
			diceNumber = int(args[0])
		except ValueError:
			if args[0] == "":
				diceNumber = 1
			else:
				await send(message.channel, "The !roll command must be in the form \"!roll (number of dice)d[type of dice]\", e.g. \"!roll 3d6\", \"!roll d20\", etc.")
				return

		try:
			diceType = int(args[1])
		except ValueError:
				await send(message.channel, "The !roll command must be in the form \"!roll (number of dice)d[type of dice]\", e.g. \"!roll 3d6\", \"!roll d20\", etc.")
				return

		if diceNumber > 1000:
			await send(message.channel, "The number of dice cannot exceed 1000.")
			return
		elif diceNumber < 1:
			await send(message.channel, "The number of dice must be at least 1.")
			return
		elif diceType > 1000:
			await send(message.channel, "The largest type of dice allowed is d1000.")
			return
		elif diceType < 2:
			await send(message.channel, "The smallest type of dice allowed is d2.")
			return
		else:
			dice = []

			for die in range(diceNumber):
				dice.append(random.randrange(1, diceType+1))

			total = 0
			response = "Rolled {}d{}: ".format(diceNumber, diceType)
			for die in dice:
				response += str(die) + ", "
				total += die
			response += "for a total of " + str(total)

			chunks = (response[0+i:2000+i] for i in range(0, len(response), 2000)) #from stackoverflow because ofc it is
			for chunk in chunks:
				await send(message.channel, chunk)

	if command in ["flip", "coin", "coinflip", "flipcoin"]:
		try:
			coinNumber = int(parts[1])
		except:
			coinNumber = 1

		if coinNumber > 1000:
			await send(message.channel, "The number of flips cannot exceed 1000.")
			return
		elif coinNumber < 1:
			await send(message.channel, "The number of flips must be at least 1.")
			return
		else:
			response = ""
			for i in range(coinNumber):
				response += random.choice(["Heads", "Tails"]) + ", "
			response = response[:-2]

			chunks = (response[0+i:2000+i] for i in range(0, len(response), 2000)) #from stackoverflow because ofc it is
			for chunk in chunks:
				await send(message.channel, chunk)
	
	if command in ["help", "commands"]:
		if len(parts) < 2:
			if message.author.id in mods:
				mod_cmds = [
					"\nModerator commands are:",
					"**!channel**: Sets which channel is used for various things",
					"**!pack**, **!box**: Puts a user or users in the box",
					"**!unpack**, **!unbox**: Takes a user or users out of the box"
				]
			else:
				mod_cmds = []
				
			normal_cmds = [
				"Commands are:",
				"**!xd**: <a:xd:598633289120612365>",
				"**!ngeng**: Ngengifies text",
				"**!roll**: Rolls dice",
				"**!flip**, **!coin**, **!coinflip**, **!flipcoin**: Flips coins",
				"**!tz**: Sets timezone role",
				"**!help**, **!commands**: Info/help with commands",
				"Type **!help [command]** for more detailed information on a command."
			]

			help_msg = normal_cmds + mod_cmds
		else:
			arg = parts[1].strip("!")
			if arg == "xd":
				help_msg = [
					"Usage: **!xd**",
					"Returns <a:xd:598633289120612365>."
				]
			elif arg == "ngeng":
				help_msg = [
					"Usage: **!ngeng [text]**",
					"Returns ngengified text. If **!ngeng** is run with no arguments, the previous message sent in the channel will be ngengified instead."
				]
			elif arg == "roll":
				help_msg = [
					"Usage: **!roll [number]type**",
					"Rolls *number* dice of type *type* and returns their results and the total. If *number* is not specified, it defaults to 1. *type* must be a lowercase letter d followed by a number (e.g. **!roll 3d20** rolls 3 20-sided dice)."
				]
			elif arg in ["flip", "coin", "coinflip", "flipcoin"]:
				help_msg = [
					"Usage: **!{} [number]**".format(arg),
					"Flips *number* coins and returns their results. If *number* is not specified, it defaults to 1."
				]
			elif arg == "tz":
				help_msg = [
					"Usage: **!{} [tz_name]**".format(arg),
					"Sets your timezone role. This is a role that says what timezone you're in and what time it is there. *tz_name* is the name of a timezone from the tz database. If you don't know what that is, use this thing I made to find out what it is for you: http://iykury.xyz/webtoys/tzname/"
				]
				
			elif arg in ["help", "commands"]:
				help_msg = [
					"Usage: **!{} [command]**".format(arg),
					"Returns information and usage on *command*. If *command* is not specified, **!{}** returns a list of all available commands.".format(arg)
				]
			elif arg == "channel":
				help_msg = [
					"Usage: **!channel channel**",
					"Sets which channel is used for various things. *channel* must be **bordraw**, **moon**, **earth**, or **box**. Only moderators are allowed to use this command."
				]
			elif arg in ["pack", "box"]:
				help_msg = [
					"Usage: **!{} user...**".format(arg),
					"Puts *user* in the box. *user* must be a mention and not simply the user's username or nickname. Multiple users can be mentioned at once. Only moderators are allowed to use this command."
				]
			elif arg in ["unpack", "unbox"]:
				help_msg = [
					"Usage: **!{} user...**".format(arg),
					"Takes *user* out of the box. *user* must be a mention and not simply the user's username or nickname. Multiple users can be mentioned at once. Only moderators are allowed to use this command."
				]
			else:
				help_msg = [
					"**!{}** is not a recognized command. Type **!help** for a list of all available commands.".format(arg)
				]
		full_msg = ""
		for msg in help_msg:
			full_msg += msg + "\n"
		full_msg.strip("\n")
		await send(message.channel, full_msg)
	
	if message.channel == channels["people"]:
		match = re.match(r"(what are people|que sont les personnes)( https?://([0-9A-Za-z-]+\.)*[0-9A-Za-z-]+(/\S*)?)?", message.content)
		if match is None or match.span() != (0, len(message.content)):
			await delete(message)
	
	if message.channel == channels["peoplediscussion"]:
		global peopleMsg
		notSent = False
		async for message in channels["people"].history(limit=1):
			if peopleMsg is None or message.id != peopleMsg.id:
				peopleMsg = message 
				notSent = True

		if notSent:
			#Download all of the attachments
			attachments = []
			os.mkdir("attachments")
			for attachment in peopleMsg.attachments:
				filepath = "attachments/" + attachment.filename
				if os.path.exists(filepath):
					number = 0
					while os.path.exists(filepath + str(number)):
						number += 1
					filepath = filepath + str(number)
				await attachment.save(filepath)
				attachments.append(discord.File(filepath))

			#Send the message
			await send(channels["peoplediscussion"], "↑Said in reply to \"{}\"".format(peopleMsg.content), files=attachments)

			#Delete the files
			for f in os.listdir("attachments"):
				os.remove("attachments/" + f)
			os.rmdir("attachments")

	if command == "tz":
		zone_name = parts[1]
		zone = tz.gettz(zone_name)
		if type(zone) is tz.tzfile:
			#Remove existing timezone roles if any
			for role in message.author.roles:
				if role.name[0] in clock_emoji:
					await message.author.remove_roles(role)

			tz_role = None
			for role in message.author.guild.roles:
				if role.name[0] in clock_emoji:
					role_zone_name = role.name[10:]
					if role_zone_name == zone_name:
						tz_role = role
						break
			if tz_role is None:
				now = time.time()
				time5 = math.floor(now/300)*300
				role_name = get_tz_role_name(zone, zone_name, time5)
				tz_role = await message.author.guild.create_role(name=role_name)
			await message.author.add_roles(tz_role)
		else:
			await send(message.channel, "*Error: {} is an invalid timezone name*".format(zone_name))

	#Bordraw
	if message.channel == bordraw.channel:
		await bordraw.on_message(message)

@client.event
async def on_guild_role_create(role):
	global roles
	roles[role.name] = role

@client.event
async def on_guild_role_delete(role):
	global roles
	roles.pop(role.name)

@client.event
async def on_guild_role_update(before, after):
	global roles
	roles[after.name] = after
	if roles.get(before.name) is None:
		log("Role {} not found".format(before.name))
	else:
		roles.pop(before.name)
	

async def earth_name():
	earth_channel_names = "🌍🌎🌏"
	while True:
		log("earth_name starting", True)
		if channels["earth"] is not None:
			hour = time.gmtime().tm_hour
			if hour >= 7 and hour < 15:
				name_index = 0
			elif hour >= 15 and hour < 23:
				name_index = 1
			else:
				name_index = 2
			log("earth_name: " + "{}conworlds".format(earth_channel_names[name_index]), True)
			try:
				await channels["earth"].edit(name="{}conworlds".format(earth_channel_names[name_index]))
			except Exception as e:
				log("Failed to edit channel name: " + e)
		await asyncio.sleep(300)

async def moon_name():
	moon_channel_names = "🌑🌒🌓🌔🌕🌖🌗🌘🌚🌝"
	while True:
		log("moon_name starting", True)
		if channels["moon"] is not None:
			now = time.time() #Current Unix time
			month = 2551442.8032 #Average length of a month in seconds
			one_phase = month / 8 #Average length of a lunar phase in seconds
			new_moon = 1546738192 #Unix time of the first new moon in 2019
			since_new_moon = (now - new_moon) #Time since the first new moon in 2019 in seconds
			current_phase = round(since_new_moon / one_phase) % 8 #Current phase
			if current_phase == 0 and chance(0.01):
				current_phase = 8
			if current_phase == 4 and chance(0.01):
				current_phase = 9
			log("moon_name: " + "{}moonchannel".format(moon_channel_names[current_phase]), True)
			try:
				await channels["moon"].edit(name="{}moonchannel".format(moon_channel_names[current_phase]))
			except Exception as e:
				log("Failed to edit channel name: " + e)
		await asyncio.sleep(300)

clock_emoji = "🕛🕧🕐🕜🕑🕝🕒🕞🕓🕟🕔🕠🕕🕡🕖🕢🕗🕣🕘🕤🕙🕥🕚🕦"
async def update_tz_roles():
	while True:
		log("update_tz_roles starting", True)
		now = time.time()
		#time5 = math.ceil((now + 10)/300)*300 #Current time (+10 seconds just in case) rounded up to the nearest 5 minutes
		time5 = math.ceil((now + 10)/900)*900 #Current time (+10 seconds just in case) rounded up to the nearest 15 minutes
		await asyncio.sleep(time5 - now) #Wait until the time calculated above before proceeding
		for guild in client.guilds:
			for role in guild.roles:
				if role.name[0] in clock_emoji: #If the role starts with a clock emoji
					zone_name = role.name[10:] #The first part of the role name is always 10 characters, so this gets the timezone
					zone = tz.gettz(zone_name)
					if type(zone) is tz.tzfile:
						role_name = get_tz_role_name(zone, zone_name, time5)
						log("update_tz_roles: " + role_name, True)
						try:
							await role.edit(name=role_name)
						except Exception as e:
							log("Failed to edit role: " + e)
					else:
						log("Error: {} is an invalid timezone name".format(zone_name))

def get_tz_role_name(zone, zone_name, timestamp):
	dt = datetime.fromtimestamp(timestamp, zone)
	local_time = dt.timestamp() + dt.utcoffset().seconds #Get timestamp in local time
	half_hour = round(local_time / 1800) % 24
	hour = str(dt.hour).zfill(2)
	minute = str(dt.minute).zfill(2)
	role_name = "{} [{}:{}] {}".format(clock_emoji[half_hour], hour, minute, zone_name)
	return role_name

class Dist:
	def __init__(self):
		self.chance_total = 0
		self.chance_var = random.random()
	
	def chance(self, p):
		self.chance_total += p
		if self.chance_var <= self.chance_total:
			return True

def chance(p):
	return random.random() <= p

async def moon_chat():
	while True:
		log("moon_chat starting", True)
		delay = random.randrange(4 * 3600, 12 * 3600)
		log("moon_chat: Waiting {} s".format(delay), True)
		await asyncio.sleep(delay) # Wait 4 to 12 hours
		#max() is not a clamping function; it doesn't look like Python has one built-in
		rand = random.betavariate(1.25, 10)
		if rand < 0.5:
			rand *= 2
		else:
			rand = 1
		o_num = int(rand*32)

		caps = Dist()
		plain = "M{}n".format("o"*o_num) #50% chance: Moon
		if caps.chance(0.3): #30% chance: moon
			plain = plain.lower()
		elif caps.chance(0.13): #13% chance: MOON
			plain = plain.upper()
		elif caps.chance(0.07): #7% chance: MoOn
			plain = "M" + ("oO" * int(o_num / 2)) + ("oN" if o_num % 2 == 1 else "n")
			if chance(0.5):
				plain = plain.swapcase() #MoOn -> mOoN
		
		punct = Dist()
		punctuation = ""
		if punct.chance(0.25):
			punctuation = "."
		else:
			if punct.chance(0.25):
				punctuation = "…"
			if punctuation == "…" and chance(0.5):
				pass
			else:
				length = 1
				if chance(0.25):
					length = random.randrange(2, 6)
				qmark = chance(0.3)
				alternating = chance(0.2)
				for i in range(length):
					punctuation += "?" if qmark else "!"
					if alternating:
						qmark = not qmark
		
		punctuated = plain + punctuation
		log("moon_chat: " + punctuated, True)
		await send(channels["moon"], "*{}*".format(punctuated))

roles = {}
def initialize_roles():
	global roles
	log("Registering roles:")
	server = client.guilds[0]
	for r in server.roles:
		roles[r.name] = r
		log('  "{0.name}"'.format(r))
	log("End")
	if not box_role in roles:
		log("Warning: box_role ({}) is not a registered role".format(box_role))

async def box(whom):
	await whom.add_roles(roles[box_role])
	await send(channels["box"], "{0.mention} has been packed into the box".format(whom))
	await asyncio.sleep(30 * 60)
	await unbox(whom)

async def unbox(whom):
	if roles[box_role] in whom.roles:
		await whom.remove_roles(roles[box_role])
		await send(channels["box"], "{0.mention} can now get out of the box".format(whom))
	if whom in box_futures:
		box_futures[whom].cancel()
		box_futures.pop(whom)

client.run(token)
