import asyncio, os, random, requests

channel = None
playing = False
drawing = False
voting = False
players = []
players_required = 4
submitters = []
drawings = []
all_drawings = []
game_task = None
wait_for_joins_task = None
votes = {}

#These are just placeholders while I wait for Alex to get me the actual list
map_names = {
    "white.png": "White",
    "black.png": "Black",
    "red.png": "Red",
    "orange.png": "Orange",
    "yellow.png": "Yellow",
    "chartreuse.png": "Chartreuse",
    "green.png": "Green",
    "spring_green.png": "Spring Green",
    "cyan.png": "Cyan",
    "azure.png": "Azure",
    "blue.png": "Blue",
    "purple.png": "Purple",
    "magenta.png": "Magenta",
    "rose.png": "Rose"
}

async def say(message):
    await channel.send(message)

async def on_message(message):
    global wait_for_joins_task

    if message.content.startswith("!join"):
        if playing:
            await say("Game already in progress")
        elif len(players) == 0:
            players.append(message.author)
            await say("A game will be started with {0.author.mention} as the host".format(message))
            await say("Type \"!join\" to join")
            await say("To start the game, the host needs to type \"!start\"")
            wait_for_joins_task = asyncio.Task(wait_for_joins())
            await count_remaining()
        else:
            if message.author in players:
                await say("{0.author.mention} is already on the list of players".format(message))
            else:
                wait_for_joins_task.cancel()
                players.append(message.author)
                await say("Added {0.author.mention} to the list of players".format(message))
                await say("Type \"!leave\" or \"!quit\" to leave")
                wait_for_joins_task = asyncio.Task(wait_for_joins())
            await count_remaining()
    if message.content.startswith("!leave") or message.content.startswith("!quit"):
        if message.author not in players:
            await say("{0.author.mention} isn't in the list of players".format(message))
        else:
            was_host = False
            if message.author == players[0]:
                was_host = True
            players.remove(message.author)
            await say("Removed {0.author.mention} from the list of players".format(message))
            if len(players) < players_required:
                await say("I'm sorry, but too many players have left the game, so it'll have to be ended here.")
                end_game()
                await say("Thanks for playing!")
            elif was_host:
                await say("{0.mention} is now the host".format(players[0]))
                await count_remaining()
    if message.content.startswith("!start"):
        if len(players) == 0:
            await say("Type \"!join\" to join the game, then type \"!start\" when you have enough players")
        elif message.author == players[0]:
            if len(players) < players_required:
                await count_remaining()
            else:
                global game_task
                game_task = asyncio.Task(game())
                wait_for_joins_task.cancel()
        else:
            await say("Only the host can start the game")
    if message.content.startswith("!end") or message.content.startswith("!stop"):
        await say("Ending the game...")
        end_game()
        await say("Thanks for playing!")
    if len(message.attachments) > 0 and drawing:
        url = message.attachments[0].get("url")
        filename = message.attachments[0].get("filename")
        if message.author in submitters:
            index = submitters.index(message.author)
            submitters.pop(index)
            drawings.pop(index)
        submitters.append(message.author)
        filename_prefix = 0
        prefixed_filename = filename
        while prefixed_filename in all_drawings:
            prefixed_filename = "{}_{}".format(filename_prefix, filename)
            filename_prefix += 1
        drawings.append(prefixed_filename)
        all_drawings.append(prefixed_filename)
        drawing_download = requests.get(url)
        drawing_file = open("{}/drawings/{}".format(folder, prefixed_filename), "wb")
        drawing_file.write(drawing_download.content)
        drawing_file.close()
        await say("Got drawing from {0.author.mention}".format(message))
        await message.delete()
    if message.content.startswith("!remove") and drawing:
        if message.author in submitters:
            index = submitters.index(message.author)
            submitters.pop(index)
            drawings.pop(index)
            await say("Removed {0.author.mention}'s drawing".format(message))
        else:
            await say("{0.author.mention} You can't remove your drawing if you haven't submitted one in the first place".format(message))
    if voting:
        try:
            vote = int(message.content) - 1 #Subtract 1 so the first drawing is 1, not 0
        except ValueError:
            if message.content.startswith("!unvote"):
                if message.author in votes:
                   votes.pop(message.author)
                   await say("Removed {0.author.mention}'s vote".format(message))
                else:
                    await say("{0.author.mention} You can't take away your vote if you haven't voted in the first place".format(message))
            return
        if vote < len(drawings) and vote >= 0:
            votes[message.author] = vote
            await say("Got vote from {0.author.mention}".format(message))
            await message.delete()

async def game():
    global playing
    global voting
    global submitters
    global drawings
    global all_drawings
    global drawing

    playing = True
    await say("Starting the game...")
    await say("To end the game, the host needs to type \"!end\" or \"!stop\"")
    for round_number in range(1, 6):
        submitters = []
        drawings = []
        all_drawings = []
        if not os.path.isdir("{}/drawings".format(folder)):
            os.makedirs("{}/drawings".format(folder))
        for current_drawing in os.listdir("{}/drawings".format(folder)):
            os.remove("{}/drawings/{}".format(folder, current_drawing))
        map = random.choice(os.listdir("{}/maps/".format(folder)))
        if map in map_names:
            map_name = map_names[map]
        else:
            map_name = map
        await introduce_map(round_number, map_name)
        await channel.send(file=discord.File("{}/maps/{}".format(folder, map)))
        drawing = True
        await say("To submit a drawing, send it in this channel.")
        await say("To remove your drawing, type \"!remove\"")
        await say("{} You have 5 minutes to submit your drawings".format(mention_players()))
        await asyncio.sleep(240)
        await say("{} You have 1 minute left to submit your drawings".format(mention_players()))
        await asyncio.sleep(50)
        await say("{} You have 10 seconds left to submit your drawings".format(mention_players()))
        await asyncio.sleep(10)
        await say("{} Time's up!".format(mention_players()))
        drawing = False
        voting = True
        await say("Now on to voting!")
        drawing_order = list(range(0, len(drawings))) #[0, 1, 2, 3...], one element for each drawing
        random.shuffle(drawing_order) #Randomize the order
        for drawing_index in drawing_order:
            current_drawing = drawings[drawing_order[drawing_index]] #Add 1 so the first drawing is 1, not 0
            await say("Drawing number {}:".format(drawing_index + 1))
            await channel.send(file=discord.File("{}/drawings/{}".format(folder, current_drawing)))
        await say("To vote, type the number of the drawing you're voting for")
        await say("To take away your vote, type \"!unvote\"")
        await say("You have 1 minute to vote on the drawings")
        await asyncio.sleep(50)
        await say("You have 10 seconds left to vote on the drawings")
        await asyncio.sleep(10)
        await say("Time's up!")
        voting = False
        drawing_votes = [0]*len(drawings)
        for vote in votes:
            voted_for = drawing_order[drawing_index]
            drawing_votes[voted_for] += 1
        winner = drawing_votes.index(max(drawing_votes)) #Index of the drawing with the most votes
        if drawing_votes[winner] == 1:
            await say("And the winner is... {0.mention}, with 1 vote!".format(submitters[winner]))
        else:
            await say("And the winner is... {0.mention}, with {} votes!".format(submitters[winner], drawing_votes[winner]))
        await channel.send(file=discord.File("{}/drawings/{}".format(folder, drawings[winner])))
        await asyncio.sleep(5)
    await say("That's all for now. Thanks for playing!")
    end_game()

def end_game():
    global playing
    global drawing
    global voting
    global players
    playing = False
    drawing = False
    voting = False
    players = []
    
    if wait_for_joins_task is not None:
        wait_for_joins_task.cancel()

    if game_task is not None:
        game_task.cancel()

async def count_remaining():
    remaining = players_required - len(players)
    if remaining > 1:
        await say("{} more players need to join before the game can be started".format(remaining))
    elif remaining == 1:
        await say("1 more player needs to join before the game can be started")
    else:
        await say("To start the game, the host needs to type \"!start\"")

def mention_players():
    mentionstr = ""
    for player in players:
        if player not in submitters:
            mentionstr += player.mention
    return mentionstr

def mention_all_players():
    mentionstr = ""
    for player in players:
        mentionstr += player.mention
    return mentionstr

async def introduce_map(round_number, map_name):
    if round_number == 1:
        await say("For the first round{}... {}!".format(
            random.choice([" is", ", we have", ", we begin with"]),
            map_name
        ))
    elif round_number == 2:
        await say("{} the {} round{}... {}!".format(
            random.choice(["For", "And for", "Next, for"]),
            random.choice(["next", "second"]),
            random.choice([" is", ", we have"]),
            map_name
        ))
    elif round_number == 3:
        await say("{} the {} round{}... {}!".format(
            random.choice(["For", "And for", "And now, for", "Now, for", "Next, for"]),
            random.choice(["next", "third"]),
            random.choice([" is", ", we have"]),
            map_name
        ))
    elif round_number == 4:
        await say("{} the {} round{}... {}!".format(
            random.choice(["For", "And for", "And now, for", "Now, for", "Next, for"]),
            random.choice(["next", "fourth"]),
            random.choice([" is", ", we have"]),
            map_name
        ))
    elif round_number == 5:
        await say("{} the {} round{}... {}!".format(
            random.choice(["And for", "And now for", "Now, for", "And last of all, for"]),
            random.choice(["final", "fifth and final"]),
            random.choice([" is", ", we have", ", we finish with"]),
            map_name
        ))

async def wait_for_joins():
    global players
    await asyncio.sleep(600)
    await say("{} Nobody has joined for 10 minutes, so the game has been cancelled. Sorry!".format(mention_all_players()))
    players = []
