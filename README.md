# Maidbot
This is *Maidbot* – a bot running on our Discord server.

## Dependencies

To run Maidbot, you'll need to use Python 3.5.3 or later (i actually haven't tested it with anything earlier than 3.6.8 so if it breaks for you please tell me so i can update this README). You'll also need the `discord.py`, `requests`, and `python-dateutil` modules, which can be installed using `pip`. To start the bot, put the bot's token in a file called `token`, then run `bot.py`.
